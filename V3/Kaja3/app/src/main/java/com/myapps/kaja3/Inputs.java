package com.myapps.kaja3;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Inputs extends Fragment {


    View myView;
    List<String> DB;
    int act;
    public Inputs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_inputs, container, false);
        try {
            DB = Supporter.readFromFile(getActivity().getBaseContext(),"Data.dat");
        }
        catch (Exception e){
            Supporter.writeToFile("",getActivity().getBaseContext(),"Data.dat");
            DB = Supporter.readFromFile(getActivity().getBaseContext(),"Data.dat");
        }
        act = -1;

        ((Button) myView.findViewById(R.id.mentes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ment();
            }
        });
        ((Button) myView.findViewById(R.id.torles)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                torol();
            }
        });
        ((Button) myView.findViewById(R.id.prev)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                elozo();
            }
        });
        ((Button) myView.findViewById(R.id.next)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                kovetkezo();
            }
        });
        return myView;
    }

    public void ment()
    {
        String DataLine = ((TextView)myView.findViewById(R.id.KajaNev)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.Kaloria)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.szenhidrat)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.zsir)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.feherje)).getText().toString();
                if (act == -1) {
                    DataLine+=";"+ Calendar.getInstance().getTime().getTime();
                }
                else{
                    DataLine+=DB.get(act).split(";")[5];
                }
        DB.add(DataLine);
        Supporter.writeToFile(Supporter.Convert(DB),getActivity().getBaseContext(),"Data.dat");
        ujjat();
    }

    public  void torol(){
        if(act != -1){
            DB.remove(act);
            Supporter.writeToFile(Supporter.Convert(DB),getActivity().getBaseContext(),"Data.dat");
            if (DB.size()>0 && act< DB.size()-1 && act>-1){
                loadtoView(DB.get(act));
            }
            else {
                clearpg();
            }
        }
    }

    public void kovetkezo(){
        if (act<(DB.size()-1)){
            act++;
            loadtoView(DB.get(act));
        }
    }
    public void elozo(){
        if (act > 0){
            act--;
            loadtoView(DB.get(act));
        }
        else{
            clearpg();
            act = -1;
        }
    }
    public void ujjat(){
        clearpg();
    }
    private void  clearpg()
    {
        ((TextView)myView.findViewById(R.id.feherje)).setText("");
        ((TextView)myView.findViewById(R.id.zsir)).setText("");
        ((TextView)myView.findViewById(R.id.szenhidrat)).setText("");
        ((TextView)myView.findViewById(R.id.Kaloria)).setText("");
        ((TextView)myView.findViewById(R.id.KajaNev)).setText("");
    }

    private void loadtoView(String dataline)
    {
        String[] data = dataline.split(";");
        switch (data.length-1){
            case 5: ((TextView)myView.findViewById(R.id.feherje)).setText(data[4]);
            case 4: ((TextView)myView.findViewById(R.id.zsir)).setText(data[3]);
            case 3: ((TextView)myView.findViewById(R.id.szenhidrat)).setText(data[2]);
            case 2: ((TextView)myView.findViewById(R.id.Kaloria)).setText(data[1]);
            case 1: ((TextView)myView.findViewById(R.id.KajaNev)).setText(data[0]);
        }
    }
}
