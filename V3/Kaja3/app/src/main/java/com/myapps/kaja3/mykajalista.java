package com.myapps.kaja3;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;




public class mykajalista extends Fragment {

    View myView;
    List<String> DB;

    public mykajalista() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_mykajalista, container, false);
        try {
            DB = Supporter.readFromFile(getActivity().getBaseContext(),"Data.dat");
        }
        catch (Exception e){
            Supporter.writeToFile("",getActivity().getBaseContext(),"Data.dat");
            DB = Supporter.readFromFile(getActivity().getBaseContext(),"Data.dat");
        }
        Calendar c = Calendar.getInstance();
                c.set(
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),0,0,0);
        long md =  c.getTime().getTime();
        float szkal = 0, szc = 0, szzs = 0, szf = 0;
        if (DB!=null && DB.size()>0){

            for (String s : DB){
                String[] tmp = s.split(";");
                if (tmp.length==6){
                    if(Long.valueOf(tmp[5])>md)
                    {
                        if(!tmp[1].isEmpty()) szkal += Float.valueOf(tmp[1]);
                        if(!tmp[2].isEmpty()) szc += Float.valueOf(tmp[2]);
                        if(!tmp[3].isEmpty()) szzs += Float.valueOf(tmp[3]);
                        if(!tmp[4].isEmpty()) szf += Float.valueOf(tmp[4]);
                    }
                }
            }
        }
        ((TextView)myView.findViewById(R.id.osszkal)).setText(Float.toString(szkal));
        ((TextView)myView.findViewById(R.id.osszcuki)).setText(Float.toString(szc));
        ((TextView)myView.findViewById(R.id.osszzsir)).setText(Float.toString(szzs));
        ((TextView)myView.findViewById(R.id.osszfeherje)).setText(Float.toString(szf));
        return myView;
    }

}
