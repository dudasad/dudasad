package com.myapps.kaja3;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;


public class OptionsFragment extends Fragment {

    View myView;

    public OptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_options, container, false);
/* Shared Preferencia olvasása és textviewba írása
        SharedPreferences sharedPref = getActivity().getSharedPreferences("Asd",Context.MODE_PRIVATE);
        String s;
        s =  sharedPref.getString(getResources().getString(R.string.Kor),"");
        ((TextView)myView.findViewById(R.id.kor)).setText(s);
*/      List<String> Data = null;

        try {
            Data = Supporter.readFromFile(getActivity().getBaseContext(),"config.cfg");
        }
        catch (Exception e)
        {

            Log.e("log in activity", "File not found: " + e.toString());
        }

        if(Data!=null){
            switch (Data.size()){
                case 4: ((TextView)myView.findViewById(R.id.kaloriaigeny)).setText(Data.get(3));
                case 3: ((TextView)myView.findViewById(R.id.suly)).setText(Data.get(2));
                case 2: ((Switch)myView.findViewById(R.id.nem)).setChecked( (Data.get(1).compareTo("true") == 0 ) );
                case 1: ((TextView)myView.findViewById(R.id.kor)).setText(Data.get(0));
            }
        }
        ;




        Button button = (Button) myView.findViewById(R.id.mentes);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               mentess();
            }
        });
        return myView;
    }

    public void mentess(){
        /*Shared Preferencia textviewba írása
        SharedPreferences sharedPref = getActivity().getSharedPreferences("Asd",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putString(getResources().getString(R.string.Kor), ((TextView)myView.findViewById(R.id.kor)).getText().toString()).apply();
        editor.putString(getResources().getString(R.string.Kor), ((TextView)myView.findViewById(R.id.kor)).getText().toString()).apply();
        editor.commit();*/
        String data = ((TextView)myView.findViewById(R.id.kor)).getText().toString() + "\n"
                +(((Switch)myView.findViewById(R.id.nem)).isChecked())+"\n"
                +((TextView)myView.findViewById(R.id.suly)).getText().toString()+"\n"
                +((TextView)myView.findViewById(R.id.kaloriaigeny)).getText().toString();
        Supporter.writeToFile(data,getActivity().getBaseContext(),"config.cfg");

    }
}
