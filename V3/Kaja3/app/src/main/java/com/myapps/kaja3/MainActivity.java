package com.myapps.kaja3;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fragment fragmen =  new mykajalista();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.frameLayout,fragmen).commit();
    }

    public  void clickBevitel(View view) {
        Fragment fragmen =  new Inputs();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout,fragmen).commit();
    }

    public  void clickMa(View view) {
        Fragment fragmen =  new mykajalista();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.frameLayout,fragmen).commit();
    }

    public  void clickOptions(View view) {
        Fragment fragmen =  new OptionsFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.frameLayout,fragmen).commit();
    }

}
