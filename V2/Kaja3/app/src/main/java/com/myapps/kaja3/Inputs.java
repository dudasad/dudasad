package com.myapps.kaja3;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Inputs extends Fragment {


    View myView;
    public Inputs() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_inputs, container, false);
        return myView;
    }

    public void ment(View view)
    {
        String DataLine = ((TextView)myView.findViewById(R.id.KajaNev)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.Kaloria)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.szenhidrat)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.zsir)).getText().toString()+";"+
                ((TextView)myView.findViewById(R.id.feherje)).getText().toString()+";"+ Calendar.getInstance().getTime().getTime();
    }
    private void loadtoView(String dataline)
    {
        String[] data = dataline.split(";");
        switch (data.length-1){
            case 5: ((TextView)myView.findViewById(R.id.feherje)).setText(data[4]);
            case 4: ((TextView)myView.findViewById(R.id.zsir)).setText(data[3]);
            case 3: ((TextView)myView.findViewById(R.id.szenhidrat)).setText(data[2]);
            case 2: ((TextView)myView.findViewById(R.id.Kaloria)).setText(data[1]);
            case 1: ((TextView)myView.findViewById(R.id.Kaloria)).setText(data[0]);
        }
    }
}
